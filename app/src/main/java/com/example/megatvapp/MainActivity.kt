package com.example.megatvapp

import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.*


class MainActivity : AppCompatActivity(), View.OnClickListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnInfoListener{

    //View
    private lateinit var imageView: ImageView
    //Buttons
    private lateinit var buttonWhoWe: RelativeLayout
    private lateinit var buttonOurTasks: RelativeLayout
    private lateinit var buttonOurValues: RelativeLayout
    private lateinit var buttonSustainableDevelopment: RelativeLayout
    private lateinit var videoLoader: VideoLoader

    private var selectBtnNumber = 0



    //оформляем VideoView массивом, т.к. он у нас, какбы, не будет меняться...
    private lateinit var videoView:MutableMap<Int,VideoView>

    companion object {
        const val VIDEO_STOP_DELAY:Long  = 100
        const val VIDEO_START_DELAY:Long  = 200
        const val VIDEO_HIDE_IMAGE_DELAY:Long = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        initVideoView()
        initEventButtonClick()
    }

    private fun initViews() {
        imageView = findViewById(R.id.image_view)

        buttonWhoWe = findViewById(R.id.button_who_we)
        buttonOurTasks = findViewById(R.id.button_our_tasks)
        buttonOurValues = findViewById(R.id.button_our_values)
        buttonSustainableDevelopment = findViewById(R.id.button_sustainable_development)
    }

    //фигачим МАП видосов, далее будем работать от него
    private fun initVideoView() {
        videoView= mutableMapOf()
        videoLoader = VideoLoader(this)
        for(i in 1..4){
            val video=getVideoViewByNumber(i)
            if(video!=null){
                video.setVideoURI(videoLoader.getPath(i))
                video.setOnCompletionListener(this)
                video.setOnInfoListener(this)
                videoView[i] = video
            }
        }
    }

    override fun onCompletion(mp: MediaPlayer?) {
        imageView.visibility = View.VISIBLE
    }
    override fun onInfo(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
            Handler(Looper.getMainLooper()).postDelayed({
                this.runOnUiThread {
                    imageView.visibility = View.GONE
                }
            }, VIDEO_HIDE_IMAGE_DELAY)
        }
        return false
    }

    private fun initEventButtonClick() {
        buttonWhoWe.setOnClickListener(this)
        buttonOurTasks.setOnClickListener(this)
        buttonOurValues.setOnClickListener(this)
        buttonSustainableDevelopment.setOnClickListener(this)
    }
    override fun onClick(view: View) {
        when (view.id){
            buttonWhoWe.id -> playVideo(1)
            buttonOurTasks.id -> playVideo(2)
            buttonOurValues.id -> playVideo(3)
            buttonSustainableDevelopment.id -> playVideo(4)
        }
    }
    private fun playVideo(number:Int){
        Log.d("ttt","button $number tapped")

        //первое, показываем картинку, чтоб скрыть все мелькания
        this.runOnUiThread {
            imageView.visibility=View.VISIBLE
        }

        //останавливаем старое, не страшно, у нас уже картинка есть... аха.. фик там, сделал с паузой...
        startOrStopVideo(selectBtnNumber,false)

        //говорим какое видео мы собираемся играть
        selectBtnNumber = number

        // запускаем искомое видео, пока не делаем никаких изменения
        //у нас запускается видео, и только когда INFO отработает, видео показывается на переднем фоне и в этот момент гасим картинку
        startOrStopVideo(selectBtnNumber,true)
    }

    private fun startOrStopVideo(number:Int, isStart:Boolean = true){
        this.runOnUiThread{
            if(number>0){
                val video=videoView[number]
                //ну тут понятно...
                if(isStart){
                    Handler(Looper.getMainLooper()).postDelayed({
                        this.runOnUiThread {
                            video!!.visibility=View.VISIBLE
                            video.start()
                        }
                    }, VIDEO_START_DELAY)
                }else{
                    Handler(Looper.getMainLooper()).postDelayed({
                        this.runOnUiThread{
                            video!!.pause()
                            video.seekTo(0)
                            video.visibility=View.GONE
                        }
                    }, VIDEO_STOP_DELAY)
                }
            }
        }
    }
    private fun getVideoViewByNumber(number:Int):VideoView?{
        return when (number) {
            1 ->  findViewById(R.id.video_view1)
            2 ->  findViewById(R.id.video_view2)
            3 ->  findViewById(R.id.video_view3)
            4 ->  findViewById(R.id.video_view4)
            else -> null
        }
    }


}