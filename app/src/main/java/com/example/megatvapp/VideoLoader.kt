package com.example.megatvapp

import android.content.Context
import android.net.Uri

class VideoLoader(context: Context) {

    private var videoPath = "android.resource://" + context.packageName + "/"
    fun getPath(number: Int): Uri? {
        return when (number) {
            1 -> {
                Uri.parse(videoPath + R.raw.mega1)
            }
            2 -> {
                Uri.parse(videoPath + R.raw.mega2)
            }
            3 -> {
                Uri.parse(videoPath + R.raw.mega3)
            }
            4 -> {
                Uri.parse(videoPath + R.raw.mega4)
            }
            else -> null
        }
    }
}